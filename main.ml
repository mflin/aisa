(*
  Cours "Sémantique et Application à la Vérification de programmes"

  Antoine Miné 2015
  Marc Chevalier 2018
  Ecole normale supérieure, Paris, France / CNRS / INRIA
*)

open Cfg
open Cfg_printer
open Value_domain
open Iterator

type algo = Worklist | Recursive
type values_mode = Interval_mode | Const_mode

let emit_prog  = ref false
let emit_cfg   = ref false
let value_mode = ref Interval_mode
let strat      = ref Worklist
let widening   = ref Wto.dfs_widening

(* iterate on control-flow-graph *)
let iterate cfg (module I : Iterator.Iterator) =
  let domains = I.eval_cfg !widening cfg in

  Format.printf "Count: %d@." (I.get_count ());
  Format.printf "Domains:@.";
  List.iter (fun (node, d) ->
      Format.printf "node %d: %a@." node.node_id I.D.pp_domain d)
    (List.sort (fun (n1, _) (n2, _) -> compare n1 n2) domains);

  match I.get_failed_assert () with
  | [] -> ()
  | fails -> Format.printf "Following assertions might fail:@.";
            List.iter
              (fun ext -> Format.printf "assertion %s@." (string_of_extent ext))
              fails


let select_value : values_mode -> (module Value) = function
  | Interval_mode -> (module Interval)
  | Const_mode    -> (module Const)

let select_strategie (module B : Iterator.BaseIterator)
    : (module Iterator.Iterator) =
  match !strat with
  | Worklist  -> (module Worklist.Make(B))
  | Recursive -> (module Recursive.Make(B))

(* parse filename *)
let run filename =
  let prog = File_parser.parse_file filename in
  let cfg = Tree_to_cfg.prog prog |> update_cfg in

  if !emit_prog then Printf.printf "%a" Cfg_printer.print_cfg cfg;
  if !emit_cfg  then Cfg_printer.output_dot "cfg.dot" cfg;

  let module V = (val (select_value !value_mode)) in
  let module D = Domain.Make(V) in
  let module B = Iterator.MakeBase(D) in
  let module I = (val (select_strategie (module B))) in
  iterate cfg (module I)

(* parses arguments to get filename *)
let set_value_mode = function
  | "int"   -> value_mode := Interval_mode
  | "const" -> value_mode := Const_mode
  | _ -> Printf.fprintf stderr "value mode can be int or const@.";
        exit 1

let set_strategie = function
  | "worklist"  -> strat := Worklist
  | "recursive" -> strat := Recursive
  | s -> Printf.fprintf stderr "unknown iteration strategie %s\n" s;
        exit 1

let set_algo = function
  | "dfs" -> widening := Wto.dfs_widening
  | "scc" -> widening := Wto.scc_widening
  | s -> Printf.fprintf stderr "unknown widening algo %s\n" s;
        exit 1

let options = [
    "--print-prog", Arg.Set emit_prog,         " print input file in stdout";
    "--emit-cfg",   Arg.Set emit_cfg,          " print control-flow-graph";
    "--value",      Arg.String set_value_mode, " set value mode";
    "--strategie",  Arg.String set_strategie,  " set iteration stratgie";
    "--widening-algo", Arg.String set_algo,    " set widening algo";
  ]

let usage = "usage: " ^ Sys.argv.(0) ^ "[options] files..."

let _ = Arg.parse options run usage
