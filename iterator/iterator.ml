open Cfg
open Tree_to_cfg
open Domain
open Wto

(* module giving some basic functions for each iterator implemetation *)
module type BaseIterator = sig
  module D : Domain

  val eval_inst : D.t -> inst -> D.t

  val get_failed_assert : unit -> Abstract_syntax_tree.extent list
  val get_count : unit -> int
  val eval_dom : (node, D.t) Hashtbl.t -> (D.t -> D.t -> D.t) -> D.t -> node -> D.t
end

(* module performing iteration over control flow graph
   a widening strategie is some function that build a weak topoloical order
   base on the control flow graph of the program. The widening points are
   the heads of the w.t.o. *)
module type Iterator = sig
  include BaseIterator
  val eval_cfg : (cfg -> wto) -> cfg -> (node * D.t) list
end

module MakeBase (Dom : Domain) : BaseIterator = struct

  module D = Dom

  let failed_assert = Hashtbl.create 16

  let get_failed_assert _ =
    Hashtbl.fold (fun ext _ lst -> ext :: lst) failed_assert []

  let eval_inst env = function
    | CFG_skip _        -> env
    | CFG_assign (v, e) -> D.assign env v e
    | CFG_guard guard   -> D.guard env guard
    | CFG_assert (guard, ext)  ->
       let filtered = D.guard env guard in
       if not (D.eq env filtered)
       then Hashtbl.replace failed_assert ext ();
       filtered

    | CFG_call _        -> assert false

  (* number of calls to eval dom
     help to measure performaces of strategies *)
  let eval_dom_count = ref 0
  let get_count _ = !eval_dom_count

  let eval_dom domains union base node =
    incr eval_dom_count;
    let dom = Hashtbl.find domains node in
    match node.node_in with
    | [] -> dom
    | nodes ->
       List.fold_left (fun dom arc ->
           let from = Hashtbl.find domains arc.arc_src in
           let new_dom = eval_inst from arc.arc_inst in
           union dom new_dom)
         base nodes

end

(** update cfg to handle non-recursive function calls *)
let update_cfg cfg =

  let main_func =
    List.find (fun func -> func.func_name = "main") cfg.cfg_funcs in
  add_arc cfg.cfg_init_exit main_func.func_entry (CFG_skip "goto main");

  let filter id = List.filter (fun a -> a.arc_id <> id) in

  let rebranch arc =
    let src = arc.arc_src in
    let dst = arc.arc_dst in
    match arc.arc_inst with
    | CFG_call func ->
       add_arc src func.func_entry (CFG_skip "fun call");
       add_arc func.func_exit dst (CFG_skip "fun ret");
       src.node_out <- filter arc.arc_id src.node_out;
       dst.node_in <- filter arc.arc_id dst.node_in;
    | _ -> assert false
  in

  List.iter (fun func -> List.iter rebranch func.func_calls) cfg.cfg_funcs;

  (* assume only one cfg created *)
  let rmarcs =
    List.fold_left (fun acc func -> func.func_calls @ acc) [] cfg.cfg_funcs in
  { cfg with cfg_arcs = List.filter (fun a -> not (List.mem a rmarcs)) !arcs }
