open Cfg
open Iterator
open Wto

(* recursively stabilize the sub components of every component every time
   the component is stabilized *)
module Make (B : BaseIterator) : Iterator with module D = B.D = struct

  include B

  let eval_cfg (wto_algo : cfg -> wto) (cfg : cfg) : (node * D.t) list =
    Format.printf "Eval program : Recursive algorithm@.";

    let domains : (node, D.t) Hashtbl.t = Hashtbl.create 16 in
    let init_dom = D.init cfg.cfg_vars in
    List.iter (fun node -> Hashtbl.add domains node init_dom) cfg.cfg_nodes;

    let update = B.eval_dom domains D.join init_dom in

    let rec stabilize wto =
      match wto with
      | [] -> ()
      | Node hd :: tl ->
         Format.printf "Stabilize %a@." pp_wto wto;
         let dom = Hashtbl.find domains hd in
         let new_dom = B.eval_dom domains D.widen dom hd in
         Hashtbl.replace domains hd new_dom;

         let tl = Wto.iter_head (fun n -> Hashtbl.replace domains n (update n)) tl in
         Wto.iter_tail stabilize tl;

         if not (D.eq dom new_dom)
         then stabilize wto;
      | _ -> assert false
    in

    let wto = wto_algo cfg in
    let _ = stabilize wto in

    Hashtbl.fold (fun node d lst -> (node, d) :: lst) domains []

end
