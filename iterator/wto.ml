open Cfg

type wto_elem = Node of node | Comp of wto
and wto = wto_elem list

let rec pp_wto fmt = function
  | [] -> ()
  | Node n :: [] -> Format.fprintf fmt "%d" n.node_id
  | Node n :: tl -> Format.fprintf fmt "%d %a" n.node_id pp_wto tl
  | Comp w :: [] -> Format.fprintf fmt "@{(%a)@}" pp_wto w
  | Comp w :: tl -> Format.fprintf fmt "@{(%a)@} %a" pp_wto w pp_wto tl

let heads =
  let rec aux acc = function
    | [] -> acc
    | Node n :: tl -> filter (NodeSet.add n acc) tl
    | Comp _ :: tl -> filter acc tl
  and filter acc lst =
    List.fold_left
      (fun acc e -> match e with Node _ -> acc | Comp w -> aux acc w) acc lst
  in
  filter NodeSet.empty

let rec iter f wto =
  let aux = function
    | Node n -> f n
    | Comp wto -> iter f wto
  in
  List.iter aux wto

let iter_comp f wto =
  let aux = function
    | Node _ -> ()
    | Comp wto -> f wto
  in
  List.iter aux wto

let rec iter_head f = function
  | Node n :: tl -> f n; iter_head f tl
  | tl -> tl

let rec iter_tail f = function
  | Comp wto :: tl -> f wto; iter_tail f tl
  | tl -> f tl

type node_status = Fresh | InWork | Done

(** create a w.t.o based on a simple dfs
    assume the cfg is transformed to handle function
    ie. all nodes are reachable from cfg.cfg_init_entry *)
let dfs_widening cfg =
  let colors = Hashtbl.create 16 in
  List.iter (fun node -> Hashtbl.add colors node Fresh) cfg.cfg_nodes;

  let rec dfs (set, order) node =
    let func ((set, order) as acc) arc =
      let dst = arc.arc_dst in
      match Hashtbl.find colors dst with
      | Fresh -> dfs acc dst
      | InWork -> NodeSet.add dst set, order
      | Done -> set, order
    in

    Hashtbl.replace colors node InWork;
    let set, order = List.fold_left func (set, order) node.node_out in
    Hashtbl.replace colors node Done;
    set, node :: order
  in

  (* return set of heads and reverse discover order *)
  let set, order = dfs (NodeSet.empty, []) cfg.cfg_init_entry in
  let order = List.rev order in

  (* build wto corresponding to dfs order *)
  List.fold_left (fun wto node ->
      if NodeSet.mem node set
      then Comp (Node node :: wto) :: []
      else Node node :: wto) [] order

(** compute wto base on strongly connected components
    based on tarjan algo, applied recursivly on each scc *)
let scc_widening cfg =
  let i = ref 0 in
  let n = 1 + List.fold_left (fun m x -> max m x.node_id) (-1) cfg.cfg_nodes in
  let dfn = Array.make (n+1) 0 in
  let s = ref [] in

  (* setup stack for recursive exploration of scc *)
  let setup v =
    let rec aux = function
      | [] -> []
      | x :: xs when x.node_id = v.node_id ->
         dfn.(v.node_id) <- Int.max_int;
         xs
      | x :: xs ->
         dfn.(x.node_id) <- 0;
         aux xs
    in
    s := aux !s;
  in

  let concat a b = match a with
    | Node _ as n :: [] -> n :: b
    | c -> Comp c :: b
  in

  let rec visit (v: node) (wto: wto) : int * wto =
    incr i;
    dfn.(v.node_id) <- !i;
    s := v :: !s;

    let head, wto =
      List.fold_left (fun (head, wto) a ->
          let u = a.arc_dst in
          let m, wto =
            if dfn.(u.node_id) = 0
            then visit u wto else dfn.(u.node_id), wto in
          if m <= head then m, wto else head, wto)
        (dfn.(v.node_id), wto) v.node_out
    in

    let wto =
      if head <> dfn.(v.node_id) then wto
      else (setup v; concat (comp v) wto)
    in

    head, wto

  and comp v =
    Node v ::
      List.fold_left (fun wto a ->
          let u = a.arc_dst in
          if dfn.(u.node_id) = 0 then (visit u [] |> snd) @ wto
          else wto) [] v.node_out
  in

  visit cfg.cfg_init_entry [] |> snd
