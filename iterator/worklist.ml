open Cfg
open Iterator
open Wto

(** Iterator base on the worklist algorithm *)

module Make (B : BaseIterator) : Iterator with module D = B.D = struct

  include B

  let eval_cfg wto_algo cfg =
    Format.printf "Eval program : Worklist algorithm@.";

    (* let count = ref 0 in *)
    let widening_pts = heads (wto_algo cfg) in
    let domains : (node, D.t)  Hashtbl.t = Hashtbl.create 16 in

    Format.printf "Widening points: ";
    NodeSet.iter (fun node -> Format.printf "%d " node.node_id) widening_pts;
    Format.printf "@.";

    let init_dom = D.init cfg.cfg_vars in
    (* Format.printf "init dom %a@." D.pp_domain init_dom; *)

    List.iter (fun node -> Hashtbl.add domains node init_dom) cfg.cfg_nodes;

    let update_join  = B.eval_dom domains D.join init_dom in
    let update_widen = B.eval_dom domains D.widen in

    (* Format.printf "dom : %a@." D.pp_domain init_dom; *)

    (* computing a new value for the node by applying the abstract
       instruction for each predecessor arc and taking the abstract
       join of the results; *)
    let update worklist node =
      (* Format.printf "Step %d: node %d@." !count node.node_id; *)
      (* incr count; *)

      let dom = Hashtbl.find domains node in
      let new_dom =
        if NodeSet.mem node widening_pts
        then update_widen dom node
        else update_join node
      in

      (* if the node's abstract value has changed,
         putting all the node's successors into the worklist; *)
      let worklist =
        if D.eq dom new_dom then worklist
        else List.fold_left
               (fun worklist a -> NodeSet.add a.arc_dst worklist)
               worklist node.node_out
      in

      Hashtbl.replace domains node new_dom;
      worklist
    in

    (* the algorithm is finished when there is no more node
       in the worklist. *)
    let rec loop worklist =
      match NodeSet.choose_opt worklist with
      | None -> worklist
      | Some node -> loop (update (NodeSet.remove node worklist) node)
    in

    (* start by global constant initialization *)
    let _ = loop (NodeSet.of_list cfg.cfg_nodes) in
    Hashtbl.fold (fun node d lst -> (node, d) :: lst) domains []

end
