void main() {
    int i = rand(-10, 10);

    // test widening with loops that might not end
    while (i <= 0) i--; // here i \in [-inf, 0]
    // here i \in [1, +inf] because of widening
    while (i > 5) i++; // here i \in [6, +inf]
    // here i \in [1, 5]
}
