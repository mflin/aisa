int x = rand(0,100);

void main()
{
    int i;
    for (i=0;i<10;i++) {
        x--;
        if (x<=0) break;
    }

    // because of widening i \in [0, +inf]

    assert (i > rand(-10, -1));
    assert(i < rand(5,10)); // assertion might fail
}
