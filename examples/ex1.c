int i = 0;
int j = i+1;

void main()
{
    int k;
    int t = rand(-10, 10);

    if (t <= 0) {
        t = -t; // \in [0, 10]
    }

    assert(t >= 0);

    if (t > 5 && t < 8) {
        k = t; // \in [6, 7]
    }

    // test bwd_bin on addition and substraction
    if (t == 5+j-1-i) {
        assert(t == 5);
        j = j + 1;
    }

    assert(j == 2 || j == 1);
    assert(j == 1); // assertion fail
}
