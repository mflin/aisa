int i, j;

int add(int x, int y) {
    return x + y;
}

void main() {
    j = rand(-10, 10);
    for (i = 0; i < 10; i++) {
        j = add(i, j); // a function call that perfectly merge with loop flow
    }
}
