// example from course 13

int x, r;

void f() {
    r = 2 * x;
    if (r > 100) { r = 0; }
}

// multiple calls to same function causes impossible executions
// and unnecessary widening (thus approximations)

void main() {
    x = rand(5, 10);
    r = -1;
    f();
    x = 80;
    f();
}
