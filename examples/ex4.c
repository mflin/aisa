int x, y;

void main() {
    x = rand(-10, 10);

    // test iteration on nested loops
    while (x < 10) {
        y = 0;
        while (y < x) {
            y++;
        }
    }
}
