NAME=analyzer.exe

all:
	@rm -f $(NAME)
	@dune build main.exe
	@ln -s _build/default/main.exe $(NAME)

clean:
	@dune clean
	@rm -f $(NAME) *.dot

re: clean all

.PHONY: all clean re
