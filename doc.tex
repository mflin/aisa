\documentclass[11pt,a4paper]{article}
\usepackage{geometry}
\geometry{left=2cm, top=2cm, right=2cm, bottom=2cm, footskip=.5cm}

\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{mathtools}
\usepackage{enumerate}
\usepackage[T1]{fontenc}
\usepackage[hidelinks]{hyperref}
\usepackage{natbib}
\usepackage{xfrac}
\usepackage{caption}
\usepackage{threeparttable}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{fancyvrb}
\usepackage{tikz}
\usetikzlibrary{calc,automata,positioning}
\usepackage{subcaption}

\usepackage[french]{babel}
\usepackage[utf8]{inputenc}

\lstset{
  language=caml,
  columns=[c]fixed,
  % basicstyle=\small\ttfamily,
  keywordstyle=\bfseries,
  upquote=true,
  commentstyle=,
  breaklines=true,
  showstringspaces=false,
  stringstyle=\color{blue},
  %% literate={'"'}{\textquotesingle "\textquotesingle}3
}


\title{Sémantique et applications à la vérification\\Rapport de projet}
\author{Maxime Flin}

\begin{document}
\maketitle

\section{Utilisation}

\paragraph{Compilation.}

La compilation se fait à l'aide de \texttt{dune}. Un \texttt{Makefile} est fourni, mais se contente de faire un appel à dune.

\paragraph{L'utilitaire de commande.}

Après avoir compilé le projet, un exécutable \texttt{analyser.exe} est produit. Il prend les options suivantes

\begin{description}
\item[\--\--print-prog] affiche le programme analysé
\item[\--\--emit-cfg] écrit un fichier \texttt{cfg.dot} représentant le graphe de contrôle du programme.
\item[\--\--value] définit le domaine de valeur abstrait. Ça peut être \texttt{const} pour le domaine des constantes ou \texttt{int} pour le domaine des intervales (Section~\ref{sec:value-dom}).
\item[\--\--strategie] définit la stratégie d'itération (\texttt{worklist} ou \texttt{recursive}) sur le cfg (Section~\ref{sec:iter}).
\item[\--\--widening-algo] définit l'algorithme utilisé (\texttt{dfs} ou \texttt{scc}) pour produire un \textit{ordre topologique faible} du cfg (Section~\ref{sec:wto})
\end{description}

La commande attends ensuite le nom des fichiers à analyser. Elle produit une description des domaines à chaque point du cfg. L'indicateur \texttt{Count} indique le nombre de fois que qu'un domaine à été calculé lors de l'analyse.

\begin{verbatim}
$ ./analyzer.exe examples/ex3.c
Eval program : Worklist algorithm
Widening points: 3 4
Count: 17
Domains:
node 1: i(1) = Ø
node 2: i(1) = [1, 5]
node 3: i(1) = (-\inf, 10]
node 4: i(1) = [1, +\inf)
node 5: i(1) = (-\inf, 0]
node 6: i(1) = [6, +\inf)
node 7: i(1) = Ø
node 8: i(1) = Ø
\end{verbatim}

S'il y en a, il indique les assertions qui pourraient échouer.

\begin{verbatim}
./analyzer.exe --widening-algo scc --strategie recursive examples/ex2.c
Eval program : Recursive algorithm
Stabilize 11 12 1 3 (5 6 8 7) 9 4 10 2
...
Count: 24
Domains:
node 1: i(2) = Ø, x(1) = [0, 100]
...
Following assertions might fail:
assertion examples/ex2.c:12.9-23
\end{verbatim}

\section{Les domaines abstraits}\label{sec:value-dom}

\subsection{Les domaines de valeurs}

Deux domaines de valeurs ont été implémentés: les constantes et les intervales. Un tel domaine correspond à un module du type \texttt{Value}.

Toutes les fonctions du module ont été implémentés (avec une précision variable) à l'exception des \texttt{bwd\_binary} pour la multiplication, la division et le modulo.

L'implémentation des constantes est assez direct donc on ne la détaille pas; le type des intervales distingue les différents types d'intervales

\begin{lstlisting}
type interval = { inf : Z.t; sup : Z.t }
type t =
  | B | T (* bottom et top *)
  | Lt of Z.t (* less than (-inf, x] *)
  | Gt of Z.t (* greater than [x, +inf) *)
  | I of interval
\end{lstlisting}

\subsection{Domaines abstrait du programme}

L'état du programme est représenté par un module implémentant l'interface \texttt{Domain}. J'en implémenté un foncteur basique qui associe à chaque variable une valeur abstraite. Cetter dernière pouvant être n'importe quoi du moment qu'elle est implémentée par un module de type \texttt{Value} comme montré à la section précédente.

Il y a eu une tentative d'utilisation d'Apron pour implémenter un domaine relationnel, mais en changeant de système sur mon ordinateur dune ne trouvait plus le module \texttt{Polka}. Par manque de temps et de motivation j'ai abandonné l'implémentation de cette extention.

\section{Itération sur le control-flow-graph}

J'ai commencé par implémenté un algorithme à base de worklist avant de lire~\cite{Bourdoncle93efficientchaotic} et d'essayer de l'implémenter aussi.

\subsection{Ordre topologique faible et point de widening}\label{sec:wto}

Calculer un ordre topologique n'est par nécessaire si on utilise l'algorithme worklist, cependant, il faut choisir les points ou l'on fait du widening. Il faut donc faire au moins un parcours en profondeur sur le graphe pour trouver tous les cycles, ce qui correspond à l'algorithme \texttt{dfs} pour calculer l'ordre topologique faible. Dans tous les cas, \textbf{les noeuds auxquels on applique du widening sont les têtes de composantes de l'ordre topologique faible}.

L'autre algorithme implémenté a pour but de produire des composantes plus petites et moins imbriquée. Il calcule avec l'algorithme de Tarjan les composantes fortement connexe du graphe récursivement pour en extraire tous les cycles.

Ainsi, sur le programme \texttt{examples/ex3.c}

\begin{lstlisting}
void main() {
    int i = rand(-10, 10);
    while (i <= 0) i--;
    while (i > 5) i++;
}
\end{lstlisting}

l'algorithme à base de parcours en profondeur produit \[ 7 8 1 (3 5 (4 6 2)) \] quand celui à base de composantes connexes produit \[ 7 8 1 (3 5) (4 6) 2 \] qui est plus précis et donc évite des calculs (couteux) de domaines non nécéssaires.

\subsection{Algorithmes d'itération sur le graphe}\label{sec:iter}

\paragraph{L'algorithme worklist.}

Celui-ci est assez simple, il maintient une liste des noeuds du graphes qui restent à calculer et, tant que cette liste est non vide, choisit un noeud évalue sont domaine et s'il a changé alors il ajoute tous ses sucesseurs dans la liste.

\paragraph{L'algorithme récursif.}

J'ai choisit d'implémenter l'algorithme récursif proposé par~\cite{Bourdoncle93efficientchaotic}.

On notera qu'il suffit pour cet algorithme de tester la stabilisation uniquement pour la tête de la composante, alors que dans l'algorithme worklist, il faut la calculer sur tous les noeuds qu'on évalue. Cette opération est assez couteuse, on gagne donc aussi un peu de ce côté là.

\section{Appels de fonctions}

L'extention que j'ai choisie d'implémenter est l'analyse inter-procédurale. Pour ce faire, je modifie le cfg pour chaque appel de fonction. Cette méthode a l'inconvénients d'ajouter des cycles qui ne devraient pas exister dès lors qu'on appelle plus d'une fois la fonction (illustré par l'exemple \texttt{examples/ex7.c} vu en cours). Les résultats obtenus par cette méthode ne sont pas satisfaisants.

\section*{Conclusion}

Pour conclure, j'ai implémenté un petit analyseur à base d'interprétation abstraite. On pourrait implémenter encore bien d'autres domaines (relationnels, produit réduit, etc\ldots) et parfaire l'implémentation proposée des intervales, mais ce projet montre bien comment les concepts vus en cours peuvent être concrètement appliqués à l'analyse de programmes.

Personnellement, je suis un petit peu déçu de ne pas avoir réussi à faire marcher \texttt{Apron}, mais ai apprécié essayer de produire une itération plus performante.

\nocite{*}
\bibliographystyle{alpha}
\bibliography{doc}

\end{document}
