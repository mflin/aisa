(*
  Cours "Sémantique et Application à la Vérification de programmes"

  Antoine Miné 2015
  Marc Chevalier 2018
  Ecole normale supérieure, Paris, France / CNRS / INRIA
*)

(*
  Signature of abstract domains representing sets of envrionments
  (for instance: a map from variable to their bounds).
 *)

open Abstract_syntax_tree
open Cfg
open Value_domain

module type Domain =
  sig

    (* type of abstract elements *)
    (* an element of type t abstracts a set of mappings from variables
       to integers
     *)
    type t

    val eq: t -> t -> bool

    (* initial environment, with all variables initialized to 0 *)
    val init: var list -> t

    (* empty set of environments *)
    val bottom: t

    (* assign an integer expression to a variable *)
    val assign: t -> var -> int_expr -> t

    (* filter environments to keep only those satisfying the boolean expression *)
    val guard: t -> bool_expr -> t

    (* abstract intersection *)
    val meet: t -> t -> t

    (* abstract join *)
    val join: t -> t -> t

    (* widening *)
    val widen: t -> t -> t

    (* whether an abstract element is included in another one *)
    val subset: t -> t -> bool

    (* whether the abstract element represents the empty set *)
    val is_bottom: t -> bool

    (* prints *)
    val print: out_channel -> t -> unit

    val pp_domain: Format.formatter -> t -> unit

  end

module Make (V : Value) : Domain = struct

  (* variables are only of type int *)
  module Env = Mapext.Make(Var)
  type t = V.t Env.t

  let rec pp_list f fmt = function
    | [] -> ()
    | x :: [] -> f fmt x
    | x :: xs -> f fmt x; Format.pp_print_string fmt ", "; pp_list f fmt xs

  let pp_domain fmt d =
    let pp_var fmt (var, value) =
      Format.fprintf fmt "%s(%d) = %a"
        var.var_name var.var_id V.pp_value value in
    let lst = Env.fold (fun x y lst -> (x, y) :: lst) d [] in
    Format.fprintf fmt "%a" (pp_list pp_var) lst

  let eq e1 e2 =
    try not (Env.exists2 (fun _ x y -> x <> y) e1 e2)
    with Invalid_argument _ -> false

  let init vars =
    let add env v = Env.add v V.bottom env in
    List.fold_left add Env.empty vars

  let bottom = Env.empty

  let rec eval_iexpr env = function
    | CFG_int_unary (op, iexpr) -> V.unary (eval_iexpr env iexpr) op
    | CFG_int_binary (op, iexpr1, iexpr2) ->
       V.binary (eval_iexpr env iexpr1) (eval_iexpr env iexpr2) op
    | CFG_int_var v -> Env.find v env
    | CFG_int_const c -> V.const c
    | CFG_int_rand (a, b) -> V.rand a b

  let assign env var iexpr = Env.add var (eval_iexpr env iexpr) env

  let meet e1 e2 =
    Env.fold (fun var value env ->
        Env.add var (V.meet value (Env.find var e2)) env) Env.empty e1

  let join x y  = Env.union (fun _ v1 v2 -> Some (V.join v1 v2)) x y
  let widen x y = Env.union (fun _ v1 v2 -> Some (V.widen v1 v2)) x y

  type evaluated_int_expr =
    | Ev_int_unary of int_unary_op * evaluated_int_expr * V.t
    | Ev_int_binary of int_binary_op * evaluated_int_expr * V.t * evaluated_int_expr * V.t
    | Ev_int_var of var * V.t
    | Ev_int_const of V.t

  (* evaluated an expression and build a tree containing value of each subterms *)
  let rec fwd_eval env = function
    | CFG_int_unary (op, e) ->
       let v, e = fwd_eval env e in
       let v = V.unary v op in
       v, Ev_int_unary (op, e, v)
    | CFG_int_binary (op, e1, e2) ->
       let v1, e1 = fwd_eval env e1 in
       let v2, e2 = fwd_eval env e2 in
       V.binary v1 v2 op, Ev_int_binary (op, e1, v1, e2, v2)
    | CFG_int_var var ->
       let v = Env.find var env in
       v, Ev_int_var (var, v)
    | CFG_int_const x ->
       let v = V.const x in
       v, Ev_int_const v
    | CFG_int_rand (x, y) ->
       let v = V.rand x y in
       v, Ev_int_const v

  (* restrict env so that e ends up in r *)
  let rec bwd_eval env e r = match e with
    | Ev_int_unary (op, e, v) -> bwd_eval env e (V.bwd_unary v op r)
    | Ev_int_binary (op, e1, v1, e2, v2) ->
       let t1, t2 = V.bwd_binary v1 v2 op r in
       bwd_eval (bwd_eval env e1 t1) e2 t2
    | Ev_int_var (var, v) ->
       let t = V.meet v r in
       if V.is_bottom t then bottom
       else Env.add var t env
    | Ev_int_const v when not (V.is_bottom (V.meet v r)) -> env
    | Ev_int_const _ -> env

  let negop = function
    | AST_EQUAL         -> AST_NOT_EQUAL
    | AST_NOT_EQUAL     -> AST_EQUAL
    | AST_LESS          -> AST_GREATER_EQUAL
    | AST_LESS_EQUAL    -> AST_GREATER
    | AST_GREATER       -> AST_LESS_EQUAL
    | AST_GREATER_EQUAL -> AST_LESS

  let rec neg = function
    | CFG_bool_const b -> CFG_bool_const (not b)
    | CFG_bool_rand -> CFG_bool_rand
    | CFG_bool_binary (AST_AND, e1, e2) ->
       CFG_bool_binary (AST_OR, neg e1, neg e2)
    | CFG_bool_binary (AST_OR, e1, e2) ->
       CFG_bool_binary (AST_AND, neg e1, neg e2)
    | CFG_bool_unary (AST_NOT, e) -> e
    | CFG_compare (op, e1, e2) -> CFG_compare (negop op, e1, e2)

  let rec guard env = function
    | CFG_bool_const true -> env
    | CFG_bool_const false -> bottom
    | CFG_bool_rand -> env
    | CFG_compare (op, e1, e2) ->
       let v1, e1 = fwd_eval env e1 in
       let v2, e2 = fwd_eval env e2 in
       let t1, t2 = V.compare v1 v2 op in

       (* Format.printf "cmp %s (%a, %a) (%a, %a)@."
        *   (Cfg_printer.string_of_compare_op op)
        *   V.pp_value v1 V.pp_value v2 V.pp_value t1 V.pp_value t2; *)

       bwd_eval (bwd_eval env e1 t1) e2 t2

    | CFG_bool_unary (AST_NOT, e) -> guard env (neg e)
    | CFG_bool_binary (AST_AND, e1, e2) -> guard (guard env e1) e2
    | CFG_bool_binary (AST_OR, e1, e2) ->
       let e1 = guard env e1 in
       let e2 = guard env e2 in
       join e1 e2

  let subset x y =
    Env.exists (fun var v1 ->
        try let v2 = Env.find var y in v1 = v2
        with Not_found -> false) x |> not

  let is_bottom env = Env.is_empty env

  let print ch d = pp_domain (Format.formatter_of_out_channel ch) d

end

(* open Apron
 * open Apron.Texpr1
 *
 * module Relational : Domain = struct
 *
 *   type man = Polka.loose Polka.t
 *   let manager = Polka.manager_alloc_loose ()
 *
 *   type t = man Abstract1.t
 *
 *   let eq = Abstract1.is_eq manager
 *   let bottom = Abstract1.bottom manager (Environment.make [||] [||])
 *
 *   let of_var v = Var.of_string (v.var_name ^ (string_of_int v.var_id))
 *
 *   let init lst =
 *     let env = Environment.make (Array.map of_var (Array.of_list lst)) [||] in
 *     Abstract1.change_environment manager bottom env false
 *
 *   let convert_binop = function
 *     | AST_PLUS     -> Add
 *     | AST_MINUS    -> Sub
 *     | AST_MULTIPLY -> Mul
 *     | AST_DIVIDE   -> Div
 *     | AST_MODULO   -> Mod
 *
 *   let rec convert_iexpr env : int_expr -> Texpr1.t = function
 *     | CFG_int_unary (AST_UNARY_PLUS, e) -> convert_iexpr env e
 *     | CFG_int_unary (AST_UNARY_MINUS, e) ->
 *        unop Neg (convert_iexpr env e) Int Zero
 *     | CFG_int_binary (op, e1, e2) ->
 *        let te1 = convert_iexpr env e1 in
 *        let te2 = convert_iexpr env e2 in
 *        binop (convert_binop op) te1 te2 Int Zero
 *     | CFG_int_var v -> var env (of_var v)
 *     | CFG_int_const x -> cst env (Coeff.s_of_int (Z.to_int x))
 *     | CFG_int_rand _ -> assert false
 *
 *   let assign dom var iexpr =
 *     let texpr = convert_iexpr (Abstract1.env dom) iexpr in
 *     Abstract1.assign_texpr manager dom (of_var var) texpr None
 *
 *   (\* exception Not_linexpr *\)
 *
 *   (\* let expand_expr expr =
 *    *   let rec aux coef = function
 *    *   | CFG_int_const x -> CFG_int_const (Z.mul x coef)
 *    *   | CFG_int_rand (a, b) -> CFG_int_rand (Z.mul a coef, Z.mul b coef)
 *    *   | CFG_int_var v when Z.equal coef Z.one -> CFG_int_var v
 *    *   | CFG_int_var v -> CFG_int_binary (AST_MULTIPLY, CFG_int_const coef, CFG_int_var v)
 *    *   | CFG_int_unary (AST_UNARY_PLUS, e) -> aux coef e
 *    *   | CFG_int_unary (AST_UNARY_MINUS, e) -> aux (Z.neg coef) e
 *    *   | CFG_int_binary (AST_MULTIPLY, CFG_int_const x, e)
 *    *     | CFG_int_binary (AST_MULTIPLY, e, CFG_int_const x) -> aux (Z.mul x coef) e
 *    *   | _ -> raise Not_linexpr
 *    *   in
 *    *   aux Z.one expr *\)
 *
 *   let expanded_expr_to_linexpr expr =
 *     let rec aux pos ((coefs, cst) as acc) = function
 *       | CFG_int_binary (AST_PLUS, e1, e2) -> aux pos (aux pos acc e1) e2
 *       | CFG_int_binary (AST_MINUS, e1, e2) -> aux (not pos) (aux pos acc e1) e2
 *       | CFG_int_binary (AST_MULTIPLY, CFG_int_const x, CFG_int_var v) ->
 *          let op = if pos then Z.add else Z.sub in
 *          begin try VarMap.add v (op (VarMap.find v coefs) x) coefs, cst
 *                with Not_found -> VarMap.add v x coefs, cst
 *          end
 *       | CFG_int_const x -> coefs, Z.add x cst
 *       | _ -> assert false
 *     in
 *     aux true (VarMap.empty, Z.zero) expr
 *
 *   (\* zarith to scalar *\)
 *   let z2s x = Z.to_int x |> Scalar.of_int |> fun x -> Coeff.Scalar x
 *
 *   let ast_op_to_lincons = function
 *     | AST_EQUAL -> Lincons1.EQ
 *     | AST_GREATER -> Lincons1.SUP
 *     | AST_GREATER_EQUAL -> Lincons1.SUPEQ
 *     | _ -> assert false
 *
 *   let build_linexpr env op coefs cst =
 *     let expr = Linexpr1.make env in
 *     let ls = VarMap.fold (fun v c ls -> (z2s c, of_var v) :: ls) coefs [] in
 *     Linexpr1.set_array expr (Array.of_list ls) (Some (z2s cst));
 *     Lincons1.make expr (ast_op_to_lincons op)
 *
 *   let guard dom = function
 *     | CFG_compare (op, e1, e2) -> (\* e1 + c1 op e2 + c2 ~> e1 - e2 op c2 - c1 *\)
 *        let coefs1, cst1 = expanded_expr_to_linexpr e1 in
 *        let coefs2, cst2 = expanded_expr_to_linexpr e2 in
 *        let coefs = VarMap.union (fun _ c1 c2 -> Some (Z.sub c1 c2)) coefs1 coefs2 in
 *        let cst = Z.sub cst2 cst1 in
 *        let _expr = build_linexpr (Abstract1.env dom) op coefs cst in
 *        dom
 *     | _ -> assert false
 *
 *   let meet = Abstract1.meet manager
 *   let join = Abstract1.join manager
 *   let widen = Abstract1.widening manager
 *   let subset = Abstract1.is_leq manager
 *   let is_bottom = Abstract1.is_bottom manager
 *   let print ch = Abstract1.print (Format.formatter_of_out_channel ch)
 *   let pp_domain = Abstract1.print
 *
 * end *)
