(*
  Cours "Sémantique et Application à la Vérification de programmes"

  Antoine Miné 2015
  Marc Chevalier 2018
  Ecole normale supérieure, Paris, France / CNRS / INRIA
*)

(*
  Signature of abstract domains representing sets of integers
  (for instance: constants or intervals).
 *)

open Abstract_syntax_tree

module type Value =
  sig

    (* type of abstract elements *)
    (* an element of type t abstracts a set of integers *)
    type t

    (* unrestricted value: [-∞,+∞] *)
    val top: t

    (* bottom value: empty set *)
    val bottom: t

    (* constant: {c} *)
    val const: Z.t -> t

    (* interval: [a,b] *)
    val rand: Z.t -> Z.t -> t


    (* unary operation *)
    val unary: t -> int_unary_op -> t

    (* binary operation *)
    val binary: t -> t -> int_binary_op -> t


    (* comparison *)
    (* [compare x y op] returns (x',y') where
       - x' abstracts the set of v  in x such that v op v' is true for some v' in y
       - y' abstracts the set of v' in y such that v op v' is true for some v  in x
       i.e., we filter the abstract values x and y knowing that the test is true

       a safe, but not precise implementation, would be:
       compare x y op = (x,y)
     *)
    val compare: t -> t -> compare_op -> (t * t)


    (* backards unary operation *)
    (* [bwd_unary x op r] return x':
       - x' abstracts the set of v in x such as op v is in r
       i.e., we fiter the abstract values x knowing the result r of applying
       the operation on x
     *)
    val bwd_unary: t -> int_unary_op -> t -> t

    (* backward binary operation *)
    (* [bwd_binary x y op r] returns (x',y') where
       - x' abstracts the set of v  in x such that v op v' is in r for some v' in y
       - y' abstracts the set of v' in y such that v op v' is in r for some v  in x
       i.e., we filter the abstract values x and y knowing that, after
       applying the operation op, the result is in r
     *)
    val bwd_binary: t -> t -> int_binary_op -> t -> (t * t)


    (* set-theoretic operations *)
    val join: t -> t -> t
    val meet: t -> t -> t

    (* widening *)
    val widen: t -> t -> t

    (* subset inclusion of concretizations *)
    val subset: t -> t -> bool

    (* check the emptiness of the concretization *)
    val is_bottom: t -> bool

    (* print abstract element *)
    val print: out_channel -> t -> unit

    val pp_value : Format.formatter -> t -> unit

  end

(** Implementation of constant domain value abstract domain *)
module Const : Value = struct

  (** an asbtraction is either empty, a constant or any constant *)
  type t = B | C of Z.t | T

  let of_ast_unop = function
    | AST_UNARY_PLUS  -> fun x -> x
    | AST_UNARY_MINUS -> Z.neg

  let of_ast_binop = function
    | AST_PLUS     -> Z.add
    | AST_MINUS    -> Z.sub
    | AST_MULTIPLY -> Z.mul
    | AST_DIVIDE   -> Z.div
    | AST_MODULO   -> Z.rem

  let of_ast_cmp = function
    | AST_EQUAL         -> Z.equal
    | AST_NOT_EQUAL     -> fun x y -> not (Z.equal x y)
    | AST_LESS          -> Z.lt
    | AST_LESS_EQUAL    -> Z.leq
    | AST_GREATER       -> Z.gt
    | AST_GREATER_EQUAL -> Z.geq

  let top    = T
  let bottom = B

  let const c  = C c
  let rand a b = if a = b then C a else T

  let unary x op = match x with
    | T | B -> x
    | C x -> C ((of_ast_unop op) x)

  let binary x y op = match x, y with
    | B, _ | _, B -> B
    | T, _ | _, T -> T
    | C a, C b -> C ((of_ast_binop op) a b)

  let meet x y = match x, y with
    | B, _ | _, B -> B
    | T, v | v, T -> v
    | C i1, C i2 when i1 = i2 -> x
    | _ -> B

  let join x y = match x, y with
    | B, v | v, B -> v
    | T, _ | _, T -> T
    | C i1, C i2 when i1 = i2 -> x
    | _ -> T

  let widen = join

  let subset x y = match x, y with
    | B, _ | _, T -> true
    | C i1, C i2 when Z.equal i1 i2 -> true
    | _    -> false

  let compare x y op = match op with
    | AST_EQUAL when subset x y -> x, x
    | AST_EQUAL when subset y x -> y, y
    | _ ->
       match x, y with
       | B, _ | _, B -> B, B
       | T, _ | _, T -> T, T (* not very precise *)
       | C i1, C i2 -> if (of_ast_cmp op) i1 i2 then x, y else B, B

  let bwd_unary x op r = match op with
    | AST_UNARY_PLUS -> meet x r
    | AST_UNARY_MINUS -> unary r AST_UNARY_MINUS

  let add x y = binary x y AST_PLUS
  let sub x y = binary x y AST_MINUS
  let bwd_add x y r = meet (sub r y) x, meet (sub r x) y
  let bwd_sub x y r = meet (add y r) x, meet (sub x r) y

  let bwd_mul _ _ _ = assert false
  let bwd_div _ _ _ = assert false
  let bwd_mod _ _ _ = assert false

  let bwd_binary x y op r = match op with
    | AST_PLUS     -> bwd_add x y r
    | AST_MINUS    -> bwd_sub x y r
    | AST_MULTIPLY -> bwd_mul x y r
    | AST_DIVIDE   -> bwd_div x y r
    | AST_MODULO   -> bwd_mod x y r

  let pp_value fmt = function
    | B   -> Format.fprintf fmt "Ø"
    | T   -> Format.fprintf fmt "T"
    | C x -> Format.fprintf fmt "%a" Z.pp_print x

  let is_bottom = function
    | B -> true
    | _ -> false

  let print chann x = pp_value (Format.formatter_of_out_channel chann) x

end

(** Implementation of interval domain value abstract domain *)
module Interval : Value = struct

  (* an abstract value is either empty or an non empty interval *)
  type interval = { inf : Z.t; sup : Z.t }
  type t =
    | B | T (* bottom et top *)
    | Lt of Z.t (* less than (-inf, x] *)
    | Gt of Z.t (* greater than [x, +inf) *)
    | I of interval

  let top    = T
  let bottom = B

  let const c  = I { inf = c; sup = c }
  let rand a b = I { inf = a; sup = b }

  let pp_value fmt = function
    | B    -> Format.fprintf fmt "Ø"
    | T    -> Format.fprintf fmt "(-∞,+∞)"
    | Lt x -> Format.fprintf fmt "(-∞, %a]" Z.pp_print x
    | Gt x -> Format.fprintf fmt "[%a, +∞)" Z.pp_print x
    | I i -> Format.fprintf fmt "[%a, %a]"
              Z.pp_print i.inf
              Z.pp_print i.sup

  let unary x = function
    | AST_UNARY_PLUS -> x
    | AST_UNARY_MINUS ->
       match x with
       | B -> B
       | T -> T
       | Lt x -> Gt x
       | Gt x -> Lt x
       | I {inf; sup} -> I { inf = Z.neg sup; sup = Z.neg inf }

  let add x y = match x, y with
    | B, _ | _, B -> B
    | T, _ | _, T -> T
    | Lt _, Gt _ | Gt _, Lt _ -> T
    | Lt x, Lt y -> Lt (Z.add x y)
    | Gt x, Gt y -> Gt (Z.add x y)
    | I i, Lt x | Lt x, I i -> Lt (Z.add x i.sup)
    | I i, Gt x | Gt x, I i -> Gt (Z.add x i.inf)
    | I i1, I i2 ->
       I { inf = Z.add i1.inf i2.inf; sup = Z.add i1.sup i2.sup }

  let sub x y = add x (unary y AST_UNARY_MINUS)

  let mul x y = match x, y with
    | B, _ | _, B -> B
    | T, _ | _, T -> T
    | _, I i when Z.equal i.inf Z.zero && Z.equal i.sup Z.zero -> I i
    | Lt x, Lt y -> if Z.leq y Z.zero && Z.leq y Z.zero then Gt (Z.mul x y) else T
    | Lt x, Gt y | Gt y, Lt x ->
       if Z.gt x Z.zero || Z.lt y Z.zero then T else Lt (Z.mul x y)
    | Lt x, I i | I i, Lt x ->
       if Z.geq i.inf Z.zero then Lt (Z.mul x i.inf)
       else if Z.gt i.sup Z.zero then T
       else Gt (Z.mul x i.inf)
    | Gt x, Gt y -> if Z.geq y Z.zero && Z.geq y Z.zero then Gt (Z.mul x y) else T
    | Gt x, I i | I i, Gt x ->
       if Z.geq i.inf Z.zero then Gt (Z.mul x i.inf)
       else if Z.gt i.sup Z.zero then T
       else Lt (Z.mul x i.inf)
    | I i1, I i2 ->
       let v1 = Z.mul i1.inf i2.inf in
       let v2 = Z.mul i1.inf i2.sup in
       let v3 = Z.mul i1.sup i2.inf in
       let v4 = Z.mul i1.sup i2.sup in
       let inf = List.fold_left Z.min v1 [v1;v2;v3;v4] in
       let sup = List.fold_left Z.max v1 [v1;v2;v3;v4] in
       I { inf; sup }

  let div x y = match x, y with
    | B, _ | _, B -> B
    | T, _ | _, T -> T
    | _ -> T

  let rem x y = match x, y with
    | B, _ | _, B -> B
    | T, _ | _, T -> T
    | _, I i ->
       if Z.gt i.inf Z.zero then I { inf = Z.zero; sup = i.sup }
       else if Z.lt i.sup Z.zero then I { inf = i.inf; sup = Z.zero }
       else let m = Z.max (Z.neg i.inf) i.sup in I { inf = Z.neg m; sup = m }
    | _ -> T

  let binary x y = function
    | AST_PLUS     -> add x y
    | AST_MINUS    -> sub x y
    | AST_MULTIPLY -> mul x y
    | AST_DIVIDE   -> div x y
    | AST_MODULO   -> rem x y

  let join x y = match x, y with
    | B, v | v, B -> v
    | Lt _, Gt _ | Gt _, Lt _ | T, _ | _, T -> T
    | Lt x, Lt y -> Lt (Z.max x y)
    | Gt x, Gt y -> Gt (Z.min x y)
    | Lt x, I i | I i, Lt x -> Lt (Z.max x i.sup)
    | Gt x, I i | I i, Gt x -> Gt (Z.min x i.inf)
    | I i1, I i2 ->
       I { inf = Z.min i1.inf i2.inf; sup = Z.max i1.sup i2.sup }

  let meet x y = match x, y with
    | B, _ | _, B -> B
    | T, x | x, T -> x
    | Lt x, Gt y when Z.lt x y -> B
    | Gt y, Lt x when Z.lt x y -> B
    | Gt y, Lt x | Lt x, Gt y -> I { inf = y; sup = x }
    | Lt x, Lt y -> Lt (Z.min x y)
    | Gt x, Gt y -> Gt (Z.max x y)
    | Lt x, I i | I i, Lt x ->
       if Z.lt x i.inf then B
       else I { inf = i.inf; sup = x }
    | Gt x, I i | I i, Gt x ->
       if Z.lt i.sup x then B
       else I { inf = x; sup = i.sup }
    | I i1, I i2 ->
       let inf = max i1.inf i2.inf in
       let sup = min i1.sup i2.sup in
       if Z.gt inf sup then B
       else I { inf; sup }

  let widen x y = match x, y with
    | B, v | v, B -> v
    | T, _ | _, T | Lt _, Gt _ | Gt _, Lt _ -> T
    | Lt x, Lt y -> if Z.geq x y then Lt x else T
    | Gt x, Gt y -> if Z.geq y x then Gt x else T
    | Lt x, I i -> if Z.geq x i.sup then Lt x else T
    | I i, Lt x -> if Z.gt x i.sup then T else Lt i.sup
    | Gt x, I i -> if Z.leq x i.inf then Gt x else T
    | I i, Gt x -> if Z.leq i.inf x then Gt i.inf else T
    | I i1, I i2 ->
       if Z.leq i1.inf i2.inf then
         if Z.geq i1.sup i2.sup then I i1
         else Gt i1.inf
       else if Z.geq i1.sup i2.sup then Lt i1.sup
       else T

  let subset x y = meet x y = x

  let eq x y = let inter = meet x y in inter, inter

  let neq x y = match x, y with
    | B, _ | _, B -> B, B
    | T, _ | _, T
      | Lt _, _ | _, Lt _
      | Gt _, _ | _, Gt _ -> x, y
    | I i1, I i2 ->
       if Z.equal i1.inf i2.inf &&
            Z.equal i1.inf i1.sup &&
              Z.equal i2.inf i2.sup
       then B, B (* [x,x] != [x,x] is always false *)
       else x, y

  let lt x y = match x, y with
    | B, _ | _, B -> B, B
    | T, _ | _, T -> x, y
    | Lt x, Lt y -> if Z.geq x y then Lt (Z.pred y), Lt y else Lt x, Lt y
    | Gt x, Gt y -> if Z.geq x y then Gt x, Gt (Z.succ x) else Gt x, Gt y
    | Lt _, Gt _ -> x, y
    | Gt x, Lt y ->
       if Z.lt x y
       then I { inf = x; sup = Z.pred y }, I { inf = (Z.succ x); sup = y}
       else B, B
    | Lt x, I i -> if Z.geq x i.sup then Lt (Z.pred i.sup), I i else Lt x, I i
    | Gt x, I i -> if Z.geq x i.sup then B, B
                  else I { inf = x; sup = Z.pred i.sup },
                       I { i with inf = Z.succ x }
    | I i, Lt x -> if Z.geq i.inf x then B, B
                  else I { i with sup = Z.min i.sup (Z.pred x) },
                       I { inf = Z.succ i.inf; sup = x }
    | I i, Gt x -> I i, Gt (Z.max x (Z.succ i.inf))
    | I i1, I i2 ->
       if Z.geq i1.inf i2.sup then B, B
       else I { inf = i1.inf; sup = Z.min i1.sup (Z.pred i2.sup) },
            I { inf = Z.max (Z.succ i1.inf) i2.inf; sup = i2.sup }

  let le x y = match x, y with
    | B, _ | _, B -> B, B
    | T, _ | _, T -> x, y
    | Lt x, Lt y -> if Z.geq x y then Lt y, Lt y else Lt x, Lt y
    | Gt x, Gt y -> if Z.geq x y then Gt x, Gt x else Gt x, Gt y
    | Lt _, Gt _ -> x, y
    | Gt x, Lt y ->
       if Z.leq x y
       then I { inf = x; sup = y }, I { inf = x; sup = y}
       else B, B
    | Lt x, I i -> if Z.geq x i.sup then Lt i.sup, I i else Lt x, I i
    | Gt x, I i -> if Z.gt x i.sup then B, B
                  else I { inf = x; sup = i.sup },
                       I { i with inf = x }
    | I i, Lt x -> if Z.gt i.inf x then B, B
                  else I { i with sup = Z.min i.sup x },
                       I { inf = i.inf; sup = x }
    | I i, Gt x -> I i, Gt (Z.max x i.inf)
    | I i1, I i2 ->
       if Z.gt i1.inf i2.sup then B, B
       else I { inf = i1.inf; sup = Z.min i1.sup i2.sup },
            I { inf = Z.max i1.inf i2.inf; sup = i2.sup }

  let gt x y = match x, y with
    | B, _ | _, B -> B, B
    | T, _ | _, T -> x, y
    | Lt x, Lt y -> if Z.gt x y then Lt x, Lt y else Lt x, Lt (Z.pred x)
    | Gt x, Gt y -> if Z.gt x y then Gt x, Gt y else Gt (Z.succ y), Gt y
    | Gt _, Lt _ -> x, y
    | Lt x, Gt y ->
       if Z.leq x y then B, B
       else I { inf = Z.succ y; sup = x }, I { inf = y; sup = Z.pred x }
    | Lt x, I i -> if Z.leq x i.inf then B, B
                  else I { inf = Z.succ i.inf; sup = x },
                       I { i with sup = Z.pred x }
    | Gt x, I i -> if Z.gt x i.sup then Gt x, I i
                  else Gt (Z.max (Z.succ i.inf) x), I { i with sup = Z.pred x }
    | I i, Lt x -> I i, Lt (Z.min x (Z.pred i.inf))
    | I i, Gt x -> if Z.geq x i.sup then B, B
                  else I { i with inf = Z.pred x },
                       I { inf = x; sup = Z.pred i.sup }
    | I i1, I i2 ->
       if Z.leq i1.sup i2.inf then B, B
       else I { i1 with inf = Z.max i1.inf (Z.succ i2.inf) },
            I { i2 with sup = Z.min i2.sup (Z.pred i1.sup) }

  let ge x y = match x, y with
    | B, _ | _, B -> B, B
    | T, _ | _, T -> x, y
    | Lt x, Lt y -> Lt x, Lt (Z.min x y)
    | Gt x, Gt y -> Gt x, Gt (Z.max x y)
    | Gt _, Lt _ -> x, y
    | Lt x, Gt y ->
       if Z.lt x y then B, B
       else I { inf = y; sup = x }, I { inf = y; sup = x }
    | Lt x, I i -> if Z.lt x i.inf then B, B
                  else I { inf = i.inf; sup = x },
                       I { i with sup = x }
    | Gt x, I i -> if Z.geq x i.sup then Gt x, I i
                  else Gt x, I { i with sup = x }
    | I i, Lt x -> I i, Lt (Z.min x i.inf)
    | I i, Gt x -> if Z.gt x i.sup then B, B
                  else I { i with inf = x }, I { inf = x; sup = i.sup }
    | I i1, I i2 ->
       if Z.lt i1.sup i2.inf then B, B
       else I { i1 with inf = Z.max i1.inf i2.inf },
            I { i2 with sup = Z.min i2.sup i1.sup }

  let compare x y = function
    | AST_EQUAL         -> eq x y
    | AST_NOT_EQUAL     -> neq x y
    | AST_LESS          -> lt x y
    | AST_LESS_EQUAL    -> le x y
    | AST_GREATER       -> gt x y
    | AST_GREATER_EQUAL -> ge x y

  let bwd_unary  x op r = match op with
    | AST_UNARY_PLUS -> meet x r
    | AST_UNARY_MINUS -> unary r AST_UNARY_MINUS

  let bwd_add x y r = meet (sub r y) x, meet (sub r x) y
  let bwd_sub x y r = meet (add y r) x, meet (sub x r) y

  (* TODO: find courage to implement *)
  let bwd_mul x y _ = x, y
  let bwd_div x y _ = x, y
  let bwd_mod x y _ = x, y

  let bwd_binary x y op r = match op with
    | AST_PLUS     -> bwd_add x y r
    | AST_MINUS    -> bwd_sub x y r
    | AST_MULTIPLY -> bwd_mul x y r
    | AST_DIVIDE   -> bwd_div x y r
    | AST_MODULO   -> bwd_mod x y r

  let is_bottom = function
    | B -> true
    | _ -> false

  let print chann x = pp_value (Format.formatter_of_out_channel chann) x

end
